package service;
import io.grpc.stub.StreamObserver;
import proto.ZodiacGrpc;
import proto.ZodiacOuterClass;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ZodiacSign extends ZodiacGrpc.ZodiacImplBase {

    public ArrayList<ZodiacList> list=new ArrayList<ZodiacList>();
 public DateOfBirth date=new DateOfBirth();

    public void Read()
    {
        try
        {
            File fileReader=new File("D:\\Info2\\CNA\\Tema2\\Tema2\\Server\\Intervals.txt");
            Scanner read= new Scanner(fileReader);
            while(read.hasNext())
            {
                String zodiac=read.next();
                int monthStart=read.nextInt();
                int dayStart=read.nextInt();
                int monthEnd=read.nextInt();
                int dayEnd= read.nextInt();
                ZodiacList object=new ZodiacList(zodiac, monthStart, dayStart,monthEnd, dayEnd);
                list.add(object);

            }
            read.close();
        }
        catch(IOException exception)
        {
            System.out.println("Fisierul nu poate fi deschis! ");
        }
    }
    public String getSign(String dateOfBirth) {
        String zodiac="j";
        int month = date.getMonth(dateOfBirth);
        int day = date.getDay(dateOfBirth);
        Read();
        for (int index=0; index< list.size(); index++)
        {
            ZodiacList sign=list.get(index);
           if((month==sign.getMonthStart() && day>= sign.getDayStart()) ||
                   (month==sign.getMonthEnd() && day<=sign.getDayEnd()))
           {
               return sign.getZodiac();
           }
        }
        return "Something went wrong!";
    }

    @Override
    public void getData(ZodiacOuterClass.CerereDate request,
                        StreamObserver<ZodiacOuterClass.PrimireDate> responseObserver) {
        ZodiacOuterClass.PrimireDate response = ZodiacOuterClass.PrimireDate.newBuilder().
                setZodiacSign(getSign(request.getDateOfBirth())).build();
        System.out.println("Date Of Birth: " + request.getDateOfBirth() + ", zodiac: " +
                response.getZodiacSign());

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }


}

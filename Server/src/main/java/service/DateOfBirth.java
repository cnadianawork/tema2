package service;

public class DateOfBirth {

    public int month;
   public int day;
    public int year;

    public int getMonth(String dateOfBirth) {

        if (dateOfBirth.charAt(2) == '/') {
            String stringMonth = dateOfBirth.substring(0, 2);
            month = Integer.parseInt(stringMonth);
        } else if (dateOfBirth.charAt(1) == '/') {
            month = Integer.parseInt(dateOfBirth.substring(0, 1));
        } else month = -1;
        return month;
    }

    public int getDay(String dateOfBirth) {

        if (dateOfBirth.charAt(2) == '/') {
            if (dateOfBirth.charAt(5) == '/') {
                String stringDay = dateOfBirth.substring(3, 5);
                day = Integer.parseInt(stringDay);
            } else if (dateOfBirth.charAt(4) == '/') {
                day = Integer.parseInt(dateOfBirth.substring(3, 4));
            } else day = -1;
        } else if (dateOfBirth.charAt(1) == '/') {
            if (dateOfBirth.charAt(3) == '/') {
                String stringDay = dateOfBirth.substring(2, 3);
                day = Integer.parseInt(stringDay);
            } else if (dateOfBirth.charAt(4) == '/') {
                day = Integer.parseInt(dateOfBirth.substring(2, 4));
            } else day = -1;
        } else day = -1;
        return day;
    }

    public int getYear(String dateOfBirth) {

        if (dateOfBirth.charAt(3) == '/') {
            String stringYear = dateOfBirth.substring(4);
            year = Integer.parseInt(stringYear);
        } else if (dateOfBirth.charAt(4) == '/') {
            String stringYear = dateOfBirth.substring(5);
            year = Integer.parseInt(stringYear);
        } else if (dateOfBirth.charAt(5) == '/') {
            String stringYear = dateOfBirth.substring(6);
            year = Integer.parseInt(stringYear);
        } else year = -1;
        return year;
    }



}

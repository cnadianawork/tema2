import java.util.Scanner;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.ZodiacGrpc;
import proto.ZodiacOuterClass;

public class Main {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",
                8999).usePlaintext().build();
        ZodiacGrpc.ZodiacBlockingStub zodiacStub = ZodiacGrpc.newBlockingStub(channel);
        Scanner scanner = new Scanner(System.in);
        int isRunning = 0;
        while(isRunning==0)
        {
            System.out.println("Date of birth: ");
            String dateOfBirth=scanner.next();
            Check ckeckDate=new Check(dateOfBirth);
            if(ckeckDate.CheckDateOfBirth()==true)
            {
                ZodiacOuterClass.PrimireDate date =
                        zodiacStub.getData(ZodiacOuterClass.CerereDate.newBuilder().
                                setDateOfBirth(dateOfBirth).build());
                System.out.println(date);
            }
            else System.out.println("Something went wrong!");
            System.out.println("Press 0 if you want to try another day or any other key to close: ");
            isRunning=scanner.nextInt();
        }
    }
}

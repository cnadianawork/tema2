import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Check {

    public String dateOfBirth;

    public Check(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean CheckDateOfBirth()
    {
        String formatString = "MM/dd/yyyy";

        try {
            SimpleDateFormat format = new SimpleDateFormat(formatString);
            format.setLenient(false);
            format.parse(dateOfBirth);
        } catch (ParseException e) {
            return false;
        } catch (IllegalArgumentException e) {
            return false;
        }

        return true;
    }
}
